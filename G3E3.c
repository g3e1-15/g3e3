//GUIA 3 EJERCICIO 3
//Permita el ingreso de 5 flotantes.
//Muestre el vector en pantalla.
//Luego multiplique todos los valores por 1.65 y vuelva a mostrarlos.

#include <stdio.h>

int main()
{
    int i;

    float A[5];

    printf("Ingrese 5 numeros:\n");

    for(i=0;i<5;i++){
        scanf("%f", &A[i]);
    }
    for(i=0;i<5;i++){
        printf("A[%d]=%f\n",i,A[i]);
    }
    for(i=0;i<5;i++){
        printf("A[%d]=%f\n",i,A[i]*1.65);
    }
}
